> ssh 安全外壳协议 是客户端和服务端交互模式，BS浏览器和服务端

1. 安装ssh

		yum install openssh-server 安装服务端,会默认安装客户端 
		service sshd start  启动
		chkconfig sshd on 设置开机运行
		ps -ef |grep ssh 查看进程
		yum install openssh-clients
2. 连接

		ssh 账号@公网ip  密码

3. ssh的config  
		
		config是方便批量管理多个ssh
		语法
		host "mogo"
				Hostname 192.168.60.77  ip
				User root 用户名
				Port 22  端口
				IdentityFile ~/.ssh/id_rsa.pub 秘钥文件路径
				IdentitiesOnly yes
		
		进入到root 下的ssh文件夹
		cd ~/.ssh/
		touch config
		vim config i esc : wq 
4. 免密登录
	
		cd ~/.ssh/
		touch authorized_keys
		vim authorized_keys i 粘贴秘钥 esc : wq
		可以设置多个
5. 改变端口号
		
		vim /etc/ssh/sshd_config
		可以同时监听多个
		重启ssh服务 
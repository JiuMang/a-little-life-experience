1. BIO  
	同步阻塞IO  
		每次创建一个线程，并发处理低，耗时久

1. NIO  
	> jdk1.4出现  

	同步非阻塞IO  
	一个client对应一个channel  
	做自己的事 需要主动去看 有无空闲
1. AIO  
	> jdk1.7

	异步非阻塞IO   
	发起请求 做自己的事 等待别人通知

----------


线程模型

多线程模型，一组NIO线程处理IO操作，Reactor线程池

主从线程模型，
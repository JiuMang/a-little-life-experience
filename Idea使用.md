1. [下载地址](https://www.jetbrains.com/idea/download/#section=windows)
![](https://i.imgur.com/3lpADug.png)
2. 修改配置文件  
	> bin>idea.properties   
	
	![](https://i.imgur.com/TUbYOa5.png)    
3. idea启动配置
	![](https://i.imgur.com/aLCLCr2.png)
4. 配置maven
	![](https://i.imgur.com/Zxg5o0D.png)
	![](https://i.imgur.com/EADnPe6.png)

> 快捷键
> ctrl + shift + n
	
	
	
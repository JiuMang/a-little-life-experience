> 基本结构为：(参数)->{语句}  
> 
    参数类型可推导时不需指定类型 (a) -> a*a
    只有一个参数且类型可推导时不需要()  a -> System.out.println(a)
    指定参数类型时，必须要有括号 (int a) -> System.out.println(a+1)
    参数可以为空 () () -> System.out.println("hello")
    只有一条语句时{}可以省略


----------

	
	示例1 创建线程
    new Thread(new Runnable() {
	    @Override
	    public void run() {
	        System.out.println(Thread.currentThread().getId());
	    }
	}).start();
	new Thread(() -> System.out.println(Thread.currentThread().getId())).start();


	示例2 自定义比较器
	Integer[] a = {1, 8, 3, 9, 2, 0, 5};
	Arrays.sort(a, new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
		    return o1 - o2;
		}
	});
	Arrays.sort(a, (o1, o2) -> o1 - o2);

----------
> 自带的函数式接口
Function<T, R> - 函数：输入 T 输出 R  
BiFunction<T, U, R> - 函数：输入 T 和 U 输出 R 对象  
Predicate<T> - 断言/判断：输入 T 输出 boolean  
BiPredicate<T, U> - 断言/判断：输入 T 和 U 输出 boolean  
Supplier<T> - 生产者：无输入，输出 T  
Consumer<T> - 消费者：输入 T，无输出  
BiConsumer<T, U> - 消费者：输入 T 和 U 无输出  
UnaryOperator<T> - 单元运算：输入 T 输出 T  
BinaryOperator<T> - 二元运算：输入 T 和 T 输出 T  
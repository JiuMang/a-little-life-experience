## 方法一： ##
导入第三方的jackson包，jackson-mapper-asl-1.9.7.jar和jackson-core-asl-1.9.7.jar

添加配置

    <mvc:annotation-driven/>
	<!-- 避免IE执行AJAX时,返回JSON出现下载文件 -->  
    <bean id="mappingJacksonHttpMessageConverter" class="org.springframework.http.converter.json.MappingJacksonHttpMessageConverter">  
        <property name="supportedMediaTypes">  
            <list>  
                <value>text/html;charset=UTF-8</value>  
            </list>  
        </property>  
    </bean>  
    <!-- 启动Spring MVC的注解功能，完成请求和注解POJO的映射 -->  
    <bean class="org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter">  
        <property name="messageConverters">  
            <list>  
                <ref bean="mappingJacksonHttpMessageConverter" /><!-- json转换器 -->  
            </list>  
        </property>  
    </bean> 

## 方法二： ##

导入第三方的fastjson包，fastjson-1.1.34.jar 

配置文件

	<mvc:annotation-driven>
		<mvc:message-converters register-defaults="true">
		    <!-- 避免IE执行AJAX时,返回JSON出现下载文件 -->
		    <bean id="fastJsonHttpMessageConverter" class="com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter">
		        <property name="supportedMediaTypes">
		            <list>
		                <value>application/json;charset=UTF-8</value>
		            </list>
		        </property>
		    </bean>
		</mvc:message-converters>
	</mvc:annotation-driven>

## 方案三 ##

导入 jackson-annotations-*.jar,jackson-core-.jar,jackson-databind-.jar 

配置文件

    <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping" />
    <bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">
        <property name="messageConverters">
            <list>
                <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                    <property name="supportedMediaTypes">
                        <list>
                            <value>text/html; charset=UTF-8</value>
                            <value>application/json;charset=UTF-8</value>
                        </list>
                    </property>
                </bean>
                <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                    <property name="supportedMediaTypes">
                        <list>
                            <value>text/html; charset=UTF-8</value>
                            <value>application/json;charset=UTF-8</value>
                        </list>
                    </property>
                </bean>
            </list>
        </property>
    </bean>